const padLeadingByChar = (number, char = '0', size = 3) => {
    let formatedStr = number + '';
    
    while (formatedStr.length < size) {
        formatedStr = char + formatedStr;
    }

    return formatedStr;
}

export {padLeadingByChar};