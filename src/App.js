import Home from './routes/home/Home';
import './main.css';
import {BrowserRouter as Router ,Route,Switch} from "react-router-dom"
import PokemonDetails from './pages/PokemonDetails';

function App() {
  return (
    <Router>
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route exact path="/pokemon/:id">
            <PokemonDetails/>
          </Route>
        </Switch>
    </Router>
    
  );
}

export default App;
