import { render } from '@testing-library/react';
import Home from './Home';
describe("Home component", () => {
    const HOME_PAGE_TITLE = "Pokemon Challenge"
    it(`Home page title should be "${HOME_PAGE_TITLE}"`, () => {
        const {getByTestId} = render(<Home />);
        const pageTitle = getByTestId("page-title");
        expect(pageTitle.innerHTML).toBe(HOME_PAGE_TITLE);
    });
})
  