import PokemonsList from '../../components/pokemon-list/PokemonsList';
function Home() {
  return (
    <>
    <h1 data-testid="page-title">Pokemon Challenge</h1>
    <div id="poke_container" className="poke-container">
        <PokemonsList/>
    </div>
    </>
  );
}

export default Home;
