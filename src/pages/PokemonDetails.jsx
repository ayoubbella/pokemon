import { useEffect,useState } from "react";
import { useParams } from "react-router-dom";
import PokemonService from '../services/PokemonService';

export default function PokemonDetails() {

  const pokemonService = new PokemonService();
  const {id} = useParams();
  const [pokemon,setPokemonDetails] = useState();

  useEffect(()=>{
    pokemonService.getPokemonById(id)
    .then(result=>{
      setPokemonDetails(result.data)
    });
  },[pokemon])

  return (
    <div>
   
    <div className="pokemonDetails">
    <aside>
      {pokemon && 
        <img
          className="card-img-details"
          src={`${process.env.REACT_APP_POKEMON_IMAGE_API}/${pokemon.id}.png`}
          alt={pokemon && pokemon.name}
        />
      }
    </aside>

    <div className="details-info">
      <h2 className="card-name">
        {pokemon && pokemon.name.toUpperCase()}
      </h2>
      <h5 className="">General Informations :</h5>
      <ul>
        <li>
          <span className=""></span>
          <span>
            Base experience : {pokemon && pokemon.base_experience}
          </span>
        </li>
        <li>
          <span className=""></span>
          <span>Height : {pokemon && pokemon.height} </span>
        </li>
        <li>
          <span className=""></span>
          <span>Weight : {pokemon && pokemon.weight} </span>
        </li>
      </ul>

      <h5 className="">Types : </h5>
      <ul>
        {pokemon && pokemon.types &&
          pokemon.types.map((type, index) => {
            return (
              <li key={index}>
                <span className=""></span>
                <span>{type.type.name}</span>
              </li>
            );
          })}
      </ul>

      <h5 className="">Moves : </h5>

      {pokemon && pokemon.moves &&
        pokemon.moves.map((move, index) => {
          if (pokemon.moves.length !== index + 1) {
            return <span key={index}>{move.move.name},</span>;
          } else {
            return <span key={index}>{move.move.name}.</span>;
          }
        })}
    </div>
  </div>
  </div>
  );
}
