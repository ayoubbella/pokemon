import { act, render , fireEvent} from '@testing-library/react';
import Pokemon from './Pokemon';

describe("Pokemon component", () => {
    const nidoking = {
        "name":"nidoking",
        "url":"https://pokeapi.co/api/v2/pokemon-species/34/"
    }

    it('should not show the pokemon modal before click', () => {
        act(() => {
            const {queryByTestId} = render(<Pokemon pokemon={nidoking} />);
            const pokemonModal = queryByTestId("pokemon-modal");
            expect(pokemonModal).toBeFalsy();
        })
    }); 

    
    it('should show pokemon modal after click', async() => {
      await  act(async () => {
            const {getByTestId, queryByTestId} = render(<Pokemon pokemon={nidoking} />);
            const pokemonCard =  getByTestId("pokemon-card");
            await fireEvent.click(pokemonCard);

            const pokemonModal = queryByTestId("pokemon-modal");

            expect(pokemonModal).toBeTruthy();
        });
    });
    
})
  