import { React,useState,useEffect, useCallback } from 'react';
import PokemonModal from '../pokemon-modal/PokemonModal'
import { padLeadingByChar } from '../../helper/NumberFormatter';
import PokemonService from '../../services/PokemonService';
import {Link } from 'react-router-dom';

export default function Pokemon({pokemon}) {
  const { name, url } = pokemon;
  const [pokemonDetails, setPokemonDetails] = useState()
  const [modalShow, setModalShow] = useState(false);
  const pokemonService = new PokemonService();

  useEffect(()=>{
    pokemonService.fetch(url)
    .then(result=>{
      setPokemonDetails(result.data)
    });
  },[])

  const setPokemonNumber = ()=>{
    if(pokemonDetails && pokemonDetails.id) return padLeadingByChar(pokemonDetails.id)
    return '';
  }

  return (
    <Link to={pokemonDetails ? "/pokemon/"+pokemonDetails.id : ''}>
    <div className="pokemon" >
        <div className="pok-div" data-testid="pokemon-card">
            <div className="img-container">
            {pokemonDetails && 
              <img src={`${process.env.REACT_APP_POKEMON_IMAGE_API}/${pokemonDetails.id}.png`} alt={name} />
            }
            </div>
            <div className="info">
              <span className="number">#{ setPokemonNumber() }</span>
              <h3 className="name">{name}</h3>
            </div>
        </div>
        
        <PokemonModal
          data-testid="pokemon-modal"
          pokemon ={pokemonDetails}
          show={modalShow}
          onHide={() => setModalShow(false)}
        /> 
        
    </div>
    </Link>

  );
}
