import { React, useState, useEffect, useRef, useCallback } from "react";
import PokemonService from "../../services/PokemonService";
import Pokemon from "../pokemon/Pokemon";


export default function PokemonsList() {
  const [pokData, setPokData] = useState();
  const [loading, setLoading] = useState(true);
  const [PokemonServiceUrl, setPokemonServiceUrl] = useState(
    process.env.REACT_APP_POKEMON_API
  );
  const [hasMore, setHasMore] = useState(false);
  const [pokemons, setPokemons] = useState([]);
  const pokemonService = new PokemonService();


  //detect if last element is visible
  const observer = useRef();
  const lastPokemonOnScreen = useCallback(
    (element) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          setPokemonServiceUrl(pokData.next); //Load next Page of Pokemons
        }
      });
      if (element) observer.current.observe(element);
    },
    [loading, hasMore]
  );

  useEffect(() => {
    setHasMore(false);
    setLoading(true);
    pokemonService.fetch(PokemonServiceUrl)
    .then(result=>{
      setLoading(false);
      setPokData(result.data);
      setHasMore(result.data !== null); //check if there is more pokemons to load
      setPokemons((prevPokemons) => {
        return [...new Set([...prevPokemons, ...result.data.results])];
      });
    });
  }, [PokemonServiceUrl]);

  return (
    <>
      {pokemons &&
        pokemons.map((pokemon, index) => {
          if (pokemons.length === index + 1) {
            return (
              <div key={index} ref={lastPokemonOnScreen}>
                <Pokemon pokemon={pokemon} />
              </div>
            );
          } else {
            return (
              <div key={index}>
                <Pokemon pokemon={pokemon} />
              </div>
            );
          }
        })}
      {loading && (
        <div className="loader">
          <img alt="loading"
            className="loading"
            src={process.env.PUBLIC_URL + "/assets/images/loader.gif"}
          ></img>
        </div>
      )}
    </>
  );
}
