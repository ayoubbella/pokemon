import axios from 'axios';

export default class PokemonService {
    
    constructor() {
      this.pokemonApiUrl = process.env.REACT_APP_POKEMON_API;
    }
  
    fetch = (url) => {
        return axios.get( url || this.pokemonApiUrl);
    };

    getPokemonById = (id) =>{
        return axios.get(this.pokemonApiUrl+'/'+id);
    }
}