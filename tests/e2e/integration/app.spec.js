it("renders without crashing", () => {
    cy.intercept('https://pokeapi.co/api/v2/pokemon').as('xhr');
    cy.visit('/')
        .get("[data-testid='pokemon-card']")
        .should("have.length", 20);
    cy.wait('@xhr');

});